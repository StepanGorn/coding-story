package com.epam.my_story;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Player_info {
    String nationality;
    String full_name;
    String nickname;
    LocalDate birth_date;
    String title;
    Map<String, String> competitions = new HashMap<>();

    public Player_info(final String nationality, final String full_name, final String nickname,
                       final LocalDate birth_date, final String title){
        this.nationality = nationality;
        this.full_name = full_name;
        this.nickname = nickname;
        this.title = title;
    }
    public String getBreed() {
        return nationality;
    }

    public String getFull_name() {
        return full_name;
    }

    public String getNickname() {
        return nickname;
    }

    public LocalDate getBirth_date() {
        return birth_date;
    }

    public String getTitle() {
        return title;
    }

    public Map<String, String> getCompetitions(){
        return Collections.unmodifiableMap(competitions);
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void addCompetitions(String contest, String mark) {
        competitions.put(contest, mark);
    }

}
