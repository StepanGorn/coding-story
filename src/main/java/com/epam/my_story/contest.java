package com.epam.my_story;

import com.epam.my_story.Titles;

import java.util.Arrays;
import java.util.List;

public class contest {
    private String name;

    private final List<Titles> possibleTitle;

    public contest(final String name, final Titles[] possibleTitle){
        this.name = name;
        this.possibleTitle = List.of(possibleTitle);
    }

    public String getName(){
        return name;
    }

    public List<Titles> possibleMarks(){
        return possibleTitle;
    }

}
