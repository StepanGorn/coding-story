package com.epam.my_story;

import com.epam.my_story.Awards;
import com.epam.my_story.Player_info;
import com.epam.my_story.Titles;
import com.epam.my_story.contest;

import java.time.LocalDate;

public class Player {
    public static void main(String[] args) {
        Player_info Russian = new com.epam.my_story.Player_info("Russian",
                "Alexander Kostilev",
                "S1mple",
                LocalDate.now().minusYears(24),
                "Best CSGO player in the world");

        Awards awards = new Awards();
        contest contest1 = new contest("IEM Winter 2021", new Titles[]{Titles.Tier1, Titles.Tier2});

        awards.addCompetitions(contest1, Titles.Tier1);
        awards.addCompetitions(contest1, Titles.Tier2);
        awards.addCompetitions(contest1, Titles.Tier1);
        awards.addCompetitions(contest1, Titles.Tier2);
        awards.addCompetitions(contest1, Titles.Tier2);
        awards.addCompetitions(contest1, Titles.Tier1);


    }
}

