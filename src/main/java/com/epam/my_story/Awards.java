package com.epam.my_story;

import com.epam.my_story.Titles;
import com.epam.my_story.contest;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Awards {
    Map<contest, Titles> competitions = new HashMap<>();

    public Map<contest, Titles> getCompetitions(){
        return Collections.unmodifiableMap(competitions);
    }

    public void addCompetitions(contest contest, Titles Title) {
        if (!contest.possibleMarks().contains(Title)){
            throw new IllegalArgumentException();
        }
        competitions.put(contest, Title);
    }


}
